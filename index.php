<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="386812564487-le26iir9cps1jn0rob3q7g5vs399r3p9.apps.googleusercontent.com">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <!--Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,800" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!--Stylesheets  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <!--Scripts  -->
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js" async></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script><!--Google Sign In  -->

    <!-- PHP Includes -->
    <?php include "php/functions.php" ?>
    <?php include "php/templates/templates.php" ?>
  </head>
  <body>

    <?php
      getHeader();
      getQuickPanel();
    ?>

    <!-- Tabs -->
    <div class="container-fluid tab-content">
      <?php
        getTab('stats');
        getTab('gear');
        getTab('abilities');
        getTab('ailments');
        getTab('admin');
        getTab('settings');
      ?>
    </div>

    <?php getFooter(); ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/classes.js"></script>
    <script src="js/injuries.js"></script>
  </body>
</html>

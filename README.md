# README #

This README is to document some basic details about the Shadows of Brimstone Companion web application.

### What is this repository for? ###

* Quick summary of the webapp
* Feature List
* Known Issues
* Listing 3rd party software used
* Disclaimer

### Quick Summary of Shadows of Brimstone Companion ###

**Q:** Who are the authors?  
**A:** Currently we have Josh Vance developing this application. Special thanks to Dre for supplying information necessary to making this work.  
  
**Q:** What is the purpose of this webapp?  
**A:** To improve the flow of the game Shadows of Brimstone.  
  
**Q:** When will the project be complete?  
**A:** Aiming to be complete before the end of July 2017.  
  
**Q:** Where will the webapp be accessed?  
**A:** I plan to host this on a subdomain of- joshvance.com.  
  
**Q:** Why is this webapp important?  
**A:** Because there are no other alternatives that provide a complete companion for Shadows of Brimstone.  

### Feature List ###

This is going to be a running list of features.
* Repo owner or admin
* Other community or team contact

### Known Issues ###
* None at the moment.

### Third Party Software ###

A special thanks to the third party software used to create this webapp!  
[Bootstrap](http://getbootstrap.com/)  
[Font Awesome](http://fontawesome.io/)  
[Google Fonts](https://fonts.google.com)  
[Initializer](http://www.initializr.com/)  
[jQuery](https://jquery.com/)  
[Modernizr](https://modernizr.com/)  

### Disclaimer ###

We are not affiliated, associated, authorized, endorsed by, or in any way officially connected with Flying Frog Productions LLC, Shadows of Brimstone, or any of its subsidiaries or affiliates.
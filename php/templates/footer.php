<!-- Footer -->
<div class="container-fluid" id="foot">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <footer>
          <p>&copy; <?php echo date('Y'); ?> Kinexus Interractive</p>
        </footer>
      </div>
    </div>
  </div>
</div>

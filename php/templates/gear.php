<!-- Gear Tab -->
<div class="container tab-pane fade" id="gear">
  <div class="row">
    <div class="col-sm-4">
      <h2>Equipped Gear</h2>

      <!-- Hat -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-male" aria-hidden="true"></i> Hat</label><br />
          <input type="text" placeholder="Hat" value="None" name="character.items.equipped.hat" />
        </div>
      </div>

      <!-- Shoulders -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-male" aria-hidden="true"></i> Shoulders</label><br />
          <input type="text" placeholder="Shoulders" value="None" name="character.items.equipped.shoulders" />
        </div>
      </div>

      <!-- Torso -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-male" aria-hidden="true"></i> Torso</label><br />
          <input type="text" placeholder="Torso" value="None" name="character.items.equipped.torso" />
        </div>
      </div>

      <!-- Coat -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-male" aria-hidden="true"></i> Coat</label><br />
          <input type="text" placeholder="Coat" value="None" name="character.items.equipped.coat" />
        </div>
      </div>

      <!-- Equip Gear -->
      <div class="row" id="stat-row">
        <button>Equip Gear</button>
      </div>
    </div>

    <div class="col-sm-4">
      <h2>Sidebag</h2>
      <div class="progress">
        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
          0/5
        </div>
      </div>
      <button>Add Sidebag Item</button>
    </div>

    <div class="col-sm-4">
      <h2>Gear</h2>
      <div class="progress">
        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
          0/5
        </div>
        <input type="text" class="form-control" placeholder="Gear Keyword Filter" aria-describedby="Gear Keyword Filter">
      </div>
      <button>Add Gear Item</button>
    </div>

  </div>
</div>

<?php

// Get Header
function getHeader() { include "php/templates/header.php"; }

// Get Quick Panel
function getQuickPanel() { include "php/templates/quick-panel.php"; }

// Get Footer
function getFooter() { include "php/templates/footer.php"; }

// Get Tab
function getTab($tabName) {
  switch($tabName) {
    case "stats":
      include "php/templates/stats.php";
      break;

    case "gear":
      include "php/templates/gear.php";
      break;

    case "abilities":
      include "php/templates/abilities.php";
      break;

    case "ailments":
      include "php/templates/ailments.php";
      break;

    case "admin":
      include "php/templates/admin.php";
      break;

    case "settings":
      include "php/templates/settings.php";
      break;
  }
}

?>

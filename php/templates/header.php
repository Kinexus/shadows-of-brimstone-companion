<!-- Navbar -->
<nav class="navbar navbar-inverse" role="navigation">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <a class="navbar-brand" href="http://www.sobcompanion.com">Shadows of Brimstone Companion</a>
      </div>
      <div class="col-sm-6">
        <div class="g-signin2 navbar-right" data-onsuccess="onSignIn"></div>
      </div>
    </div>
  </div>
</nav>

<!-- Tabs Bar -->
<div class="container-fluid" id="tabs">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#stats">Stats</a></li>
          <li><a data-toggle="tab" href="#gear">Gear</a></li>
          <li><a data-toggle="tab" href="#abilities">Abilities</a></li>
          <li><a data-toggle="tab" href="#ailments">Ailments</a></li>
          <li id="settings-tab"><a data-toggle="tab" href="#settings"><i class="fa fa-sliders" aria-hidden="true">&nbsp;</i></a></li>
          <li id="admin-tab"><a data-toggle="tab" href="#admin"><i class="fa fa-address-book" aria-hidden="true"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!-- Stats Tab -->
<div class="container tab-pane fade in active" id="stats">

  <!-- First Row -->
  <div class="row" id="stat-group">
    <div class="col-sm-12">
      <!-- Tab Title -->
      <h2>Character Stats</h2>
    </div>
    <div class="col-sm-4">
      <!-- Name -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-user-circle" aria-hidden="true"></i> Name</label><br />
          <input id="name-field" type="text" placeholder="Name" value="Kinexus" name="name" />
        </div>
      </div>

      <!-- Class -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-male" aria-hidden="true"></i> Class</label><br />
          <select name="class" onchange="applyClass(this);">
            <option value="None">Select Class</option>
            <option value="bandito">Bandito</option>
            <option value="gunslinger">Gunslinger</option>
            <option value="uSMarshal">U.S. Marshal</option>
            <option value="saloonGirl">Saloon Girl</option>
            <option value="pianoPlayer">Piano Player</option>
          </select>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <!-- Level -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-line-chart" aria-hidden="true"></i> Level</label><br />
          <input id="level" type="text" placeholder="Level" value="1" name="level" />
        </div>
      </div>

      <!-- Experience -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-star" aria-hidden="true"></i> Experience</label><br />
          <input id="experience" type="text" placeholder="experience" value="0" name="experience" />
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <!-- Gold -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-usd" aria-hidden="true"></i> Gold</label><br />
          <input id="gold" type="text" placeholder="Gold" value="0" name="gold" />
        </div>
      </div>

      <!-- Darkstone -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-diamond" aria-hidden="true"></i> Dark Stone</label><br />
          <input id="darkstone" type="text" placeholder="Darkstone" value="0" name="darkstone" />
        </div>
      </div>
    </div>
  </div>

  <!-- Second Row -->
  <div class="row" id="stat-group">
    <!-- First Column -->
    <div class="col-sm-4">
      <!-- Health -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-plus-circle" aria-hidden="true"></i> Health</label><br />
          <input id="health" type="text" placeholder="Health" value="0" name="health" />
        </div>
        <!--<div class="col-xs-4">
            <p class="status-adjustment"><a href="#" class="text-danger" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Internal Bleeding: -2 hp">injury</a></p>
            <p class="status-adjustment"><a href="#" class="text-danger" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="The Itching: -1 hp at the start of each turn">madness</a></p>
        </div>-->
      </div>

      <!-- Defense -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-shield" aria-hidden="true"></i> Defense</label><br />
          <input id="defense" type="text" placeholder="Defense" value="0" name="defense" />
        </div>
      </div>

      <!-- Sanity -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-smile-o" aria-hidden="true"></i> Sanity</label><br />
          <input id="sanity" type="text" placeholder="Sanity" value="0" name="sanity" />
        </div>
      </div>

      <!-- Willpower -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-bolt" aria-hidden="true"></i> Willpower</label><br />
          <input id="willpower" type="text" placeholder="Willpower" value="0" name="willpower" />
        </div>
      </div>
    </div>
    <!-- Second Column -->
    <div class="col-sm-4">
      <!-- Combat -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-crosshairs" aria-hidden="true"></i> Combat</label><br />
          <input id="combat" type="text" placeholder="Combat" value="0" name="combat" />
        </div>
      </div>

      <!-- Grit -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-asterisk" aria-hidden="true"></i> Grit (Max)</label><br />
          <input id="grit" type="text" placeholder="Grit" value="0 (0)" name="character.stats.grit" />
        </div>
      </div>

      <!-- Range -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Range</label><br />
          <input id="range" type="text" placeholder="Range" value="0" name="range" />
        </div>
      </div>

      <!-- Melee -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-hand-rock-o" aria-hidden="true"></i> Melee</label><br />
          <input id="melee" type="text" placeholder="Melee" value="0" name="melee" />
        </div>
      </div>
    </div>
    <!-- Third Column -->
    <div class="col-sm-4">
      <!-- Initiative -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-handshake-o" aria-hidden="true"></i> Initiative</label><br />
          <input id="initiative" type="text" placeholder="Initiative" value="0" name="initiative" />
        </div>
      </div>

      <!-- Corruption -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-bomb" aria-hidden="true"></i> Corruption</label><br />
          <input type="text" placeholder="Corruption" value="0" name="character.stats.corruption" />
        </div>
      </div>

      <!-- Move -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-arrows" aria-hidden="true"></i> Move</label><br />
          <input type="text" placeholder="Move" value="D6" name="character.stats.move" />
        </div>
      </div>
    </div>
  </div>

  <!-- Third Row -->
  <div class="row" id="stat-group">
    <div class="col-sm-4">
      <!-- Agility -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-fighter-jet" aria-hidden="true"></i> Agility</label><br />
          <input id="agility" type="text" placeholder="agility" value="0" name="agility" />
        </div>
      </div>

      <!-- Strength -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-gavel" aria-hidden="true"></i> Strength</label><br />
          <input id="strength" type="text" placeholder="strength" value="0" name="strength" />
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <!-- Cunning -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-github-alt" aria-hidden="true"></i> Cunning</label><br />
          <input id="cunning" type="text" placeholder="cunning" value="0" name="cunning" />
        </div>
      </div>

      <!-- Lore -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-book" aria-hidden="true"></i> Lore</label><br />
          <input id="lore" type="text" placeholder="lore" value="0" name="lore" />
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <!-- Spirit -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-child" aria-hidden="true"></i> Spirit</label><br />
          <input id="spirit" type="text" placeholder="spirit" value="0" name="spirit" />
        </div>
      </div>

      <!-- Luck -->
      <div class="row" id="stat-row">
        <div class="col-xs-8">
          <label><i class="fa fa-money" aria-hidden="true"></i> Luck</label><br />
          <input id="luck" type="text" placeholder="luck" value="0" name="luck" />
        </div>
      </div>
    </div>
  </div>
</div>

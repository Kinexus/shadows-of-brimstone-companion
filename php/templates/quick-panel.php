<!-- Quick Panel -->
<div class="container-fluid" id="quick-panel">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul>
          <li><i class="fa fa-plus-circle" aria-hidden="true"></i> Health: 0</li>
          <li><i class="fa fa-shield" aria-hidden="true"></i> Defense: 0</li>
          <li><i class="fa fa-archive" aria-hidden="true"></i> Sidebag: 0/5</li>
          <li><i class="fa fa-frown-o" aria-hidden="true"></i> Ailments:
            <a href="#" class="text-danger" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Internal Bleeding: -2 hp">injury</a>,
            <a href="#" class="text-danger" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="The Itching: -1 hp at the start of each turn">madness</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

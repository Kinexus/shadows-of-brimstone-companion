<!-- Ailments Tab -->
<div class="container tab-pane fade" id="ailments">
  <div class="row">

    <!-- Injuries -->
    <div class="col-sm-4">
      <h2>Injuries</h2>
      <button>Add Injury</button>
    </div>

    <!-- Madness -->
    <div class="col-sm-4">
      <h2>Madness</h2>
      <button>Add Madness</button>
    </div>

    <!-- Mutations -->
    <div class="col-sm-4">
      <h2>Mutations</h2>
      <button>Add Mutation</button>
    </div>

  </div>
</div>

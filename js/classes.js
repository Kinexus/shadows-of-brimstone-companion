var classes =  {"bandito":{"classname":"Bandito","keywords":{"keyword0":"Outlaw"},"health":"16","defense":"4","sanity":"8","willpower":"5","grit":"2","combat":"2","initiative":"3","melee":"4","range":"5","agility":"2","cunning":"1","spirit":"3","strength":"4","lore":"3","luck":"2","startingItems":{"startingItem0":"Pistol"},"startingAbilities":{"startingAbility0":"Wild Card"}},"gunslinger":{"classname":"Gunslinger","keywords":{"keyword0":"Showman"},"health":"10","defense":"5","sanity":"12","willpower":"4","grit":"2","combat":"1","initiative":"6","melee":"5","range":"3","agility":"3","cunning":"3","spirit":"2","strength":"2","lore":"2","luck":"4","startingItems":{"startingItem0":"Pistol"},"startingAbilities":{"startingAbility0":"Quick and the Dead"}},"uSMarshal":{"classname":"U.S. Marshal","keywords":{"keyword0":"Law","keyword1":"Traveler"},"health":"10","defense":"3","sanity":"10","willpower":"4","grit":"2","combat":"2","initiative":"4","melee":"4","range":"4","agility":"3","cunning":"4","spirit":"2","strength":"2","lore":"1","luck":"3","startingItems":{"startingItem0":"Shotgun","startingItem1":"US Marshal Badge"},"startingAbilities":{"startingAbility0":"Quick and the Dead"}},"saloonGirl":{"classname":"Saloon Girl","keywords":{"keyword0":"Performer"},"health":"8","defense":"3","sanity":"14","willpower":"4","grit":"2","combat":"2","initiative":"5","melee":"4","range":"4","agility":"4","cunning":"3","spirit":"3","strength":"1","lore":"2","luck":"3","startingItems":{"startingItem0":"Hold_Out Pistol"},"startingAbilities":{"startingAbility0":"Comforting Presence","startingAbility1":"Lightweight","startingAbility2":"Fast"}},"pianoPlayer":{"classname":"Piano Player","keywords":{"keyword0":"Performer"},"health":"8","defense":"3","sanity":"14","willpower":"4","grit":"2","combat":"2","initiative":"5","melee":"4","range":"4","agility":"4","cunning":"3","spirit":"3","strength":"1","lore":"2","luck":"3","startingItems":{"startingItem0":"Hold_Out Pistol"},"startingAbilities":{"startingAbility0":"Comforting Presence","startingAbility1":"Lightweight","startingAbility2":"Fast"}}};

function applyClass(objectValue) {
  var className = objectValue.value;
  if(className == `None`) {
    return;
  } else if(className == `bandito`) {
    // stats
    $(`#health`).val(classes.bandito.health);
    $(`#defense`).val(classes.bandito.defense);
    $(`#sanity`).val(classes.bandito.sanity);
    $(`#willpower`).val(classes.bandito.willpower);
    $(`#grit`).val(`0 ( ${classes.bandito.grit} )`);
    $(`#combat`).val(classes.bandito.combat);
    $(`#initiative`).val(classes.bandito.initiative);
    $(`#melee`).val(classes.bandito.melee);
    $(`#range`).val(classes.bandito.range);
    $(`#agility`).val(classes.bandito.agility);
    $(`#cunning`).val(classes.bandito.cunning);
    $(`#spirit`).val(classes.bandito.spirit);
    $(`#strength`).val(classes.bandito.strength);
    $(`#lore`).val(classes.bandito.lore);
    $(`#luck`).val(classes.bandito.luck);
    return;
  } else if(className == `gunslinger`) {
    // stats
    $(`#health`).val(classes.gunslinger.health);
    $(`#defense`).val(classes.gunslinger.defense);
    $(`#sanity`).val(classes.gunslinger.sanity);
    $(`#willpower`).val(classes.gunslinger.willpower);
    $(`#grit`).val(`0 ( ${classes.gunslinger.grit} )`);
    $(`#combat`).val(classes.gunslinger.combat);
    $(`#initiative`).val(classes.gunslinger.initiative);
    $(`#melee`).val(classes.gunslinger.melee);
    $(`#range`).val(classes.gunslinger.range);
    $(`#agility`).val(classes.gunslinger.agility);
    $(`#cunning`).val(classes.gunslinger.cunning);
    $(`#spirit`).val(classes.gunslinger.spirit);
    $(`#strength`).val(classes.gunslinger.strength);
    $(`#lore`).val(classes.gunslinger.lore);
    $(`#luck`).val(classes.gunslinger.luck);
  } else if(className == `uSMarshal`) {
    // stats
    $(`#health`).val(classes.uSMarshal.health);
    $(`#defense`).val(classes.uSMarshal.defense);
    $(`#sanity`).val(classes.uSMarshal.sanity);
    $(`#willpower`).val(classes.uSMarshal.willpower);
    $(`#grit`).val(`0 ( ${classes.uSMarshal.grit} )`);
    $(`#combat`).val(classes.uSMarshal.combat);
    $(`#initiative`).val(classes.uSMarshal.initiative);
    $(`#melee`).val(classes.uSMarshal.melee);
    $(`#range`).val(classes.uSMarshal.range);
    $(`#agility`).val(classes.uSMarshal.agility);
    $(`#cunning`).val(classes.uSMarshal.cunning);
    $(`#spirit`).val(classes.uSMarshal.spirit);
    $(`#strength`).val(classes.uSMarshal.strength);
    $(`#lore`).val(classes.uSMarshal.lore);
    $(`#luck`).val(classes.uSMarshal.luck);
  } else if(className == `saloonGirl`) {
    // stats
    $(`#health`).val(classes.saloonGirl.health);
    $(`#defense`).val(classes.saloonGirl.defense);
    $(`#sanity`).val(classes.saloonGirl.sanity);
    $(`#willpower`).val(classes.saloonGirl.willpower);
    $(`#grit`).val(`0 ( ${classes.saloonGirl.grit} )`);
    $(`#combat`).val(classes.saloonGirl.combat);
    $(`#initiative`).val(classes.saloonGirl.initiative);
    $(`#melee`).val(classes.saloonGirl.melee);
    $(`#range`).val(classes.saloonGirl.range);
    $(`#agility`).val(classes.saloonGirl.agility);
    $(`#cunning`).val(classes.saloonGirl.cunning);
    $(`#spirit`).val(classes.saloonGirl.spirit);
    $(`#strength`).val(classes.saloonGirl.strength);
    $(`#lore`).val(classes.saloonGirl.lore);
    $(`#luck`).val(classes.saloonGirl.luck);
  } else if(className == `pianoPlayer`) {
    // stats
    $(`#health`).val(classes.pianoPlayer.health);
    $(`#defense`).val(classes.pianoPlayer.defense);
    $(`#sanity`).val(classes.pianoPlayer.sanity);
    $(`#willpower`).val(classes.pianoPlayer.willpower);
    $(`#grit`).val(`0 ( ${classes.pianoPlayer.grit} )`);
    $(`#combat`).val(classes.pianoPlayer.combat);
    $(`#initiative`).val(classes.pianoPlayer.initiative);
    $(`#melee`).val(classes.pianoPlayer.melee);
    $(`#range`).val(classes.pianoPlayer.range);
    $(`#agility`).val(classes.pianoPlayer.agility);
    $(`#cunning`).val(classes.pianoPlayer.cunning);
    $(`#spirit`).val(classes.pianoPlayer.spirit);
    $(`#strength`).val(classes.pianoPlayer.strength);
    $(`#lore`).val(classes.pianoPlayer.lore);
    $(`#luck`).val(classes.pianoPlayer.luck);
  }
}

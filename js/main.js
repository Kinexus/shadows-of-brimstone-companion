$(document).ready(function() {

  // Enable Bootstrap tooltips
  $('[data-toggle="tooltip"]').tooltip();

  // Enable Bootstrap popovers
  $('[data-toggle="popover"]').popover();

  function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    $( "#name-field" ).html(BasicProfile.getGivenName());
  }

  function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
  }
});
